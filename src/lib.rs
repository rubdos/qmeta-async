//! Runtime for concurrent running of a Qt application with a Tokio runtime.

pub use qmeta_async_macros::with_executor;

use std::cell::RefCell;
use std::task::Waker;

use tokio::{runtime::Handle, task::LocalSet};

std::thread_local! {
    static LOCALSET: RefCell<LocalSet> = RefCell::new(tokio::task::LocalSet::new());
    static RUNTIME: RefCell<Option<Handle>> = RefCell::default();
    static WAKER: RefCell<Option<Waker>> = RefCell::default();
}

fn with_runtime<R, F: FnOnce(&Handle) -> R>(f: F) -> R {
    RUNTIME.with(|rt| {
        let rt = rt.borrow();
        f(rt.as_ref().expect("active runtime"))
    })
}

/// Execute closure within the running executor.
///
/// This is useful for running code in the Tokio executor, when coming from a Qt callback.
///
/// See also [with_executor].
pub fn with_executor<R, F: FnOnce() -> R>(f: F) -> R {
    with_runtime(|rt| {
        LOCALSET.with(|ls| {
            let ret = match ls.try_borrow_mut() {
                Ok(mut ls) => {
                    let ls = &mut *ls;
                    rt.block_on(ls.run_until(async move { f() }))
                }
                // Reentrant call
                Err(_borrow_mut_error) => {
                    log::debug!("Reentrant with_executor");
                    f()
                }
            };

            WAKER.with(|waker| {
                let w = waker.borrow();
                if let Some(w) = w.as_ref() {
                    w.wake_by_ref()
                }
            });

            ret
        })
    })
}

/// Entrypoint for starting Qt.
///
/// ```no_run
/// let app = qmetaobject::QmlEngine::new();
/// qmeta_async::run(|| {
///     app.exec();
/// }).expect("running application");
/// ```
pub fn run<R, F: FnOnce() -> R>(f: F) -> anyhow::Result<R> {
    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap();
    let handle = runtime.handle();
    let prev = RUNTIME.with(|rt| rt.borrow_mut().replace(handle.clone()));
    assert!(prev.is_none(), "Runtime was already initialized.");

    qmetaobject::future::execute_async(futures::future::poll_fn(move |cx| {
        with_runtime(|rt| {
            let _guard = rt.enter();
            LOCALSET.with(|ls| {
                WAKER.with(|waker| {
                    *waker.borrow_mut() = Some(cx.waker().clone());
                });

                let mut ls = ls.borrow_mut();

                // If the localset is "ready", the queue is empty.
                let _rdy = futures::ready!(futures::Future::poll(std::pin::Pin::new(&mut *ls), cx));
                log::warn!("LocalSet is empty, application will either shut down or lock up.");

                std::task::Poll::Ready(())
            })
        })
    }));

    // Spawn the Tokio I/O loop on a separate thread.
    // The first thread to call `block_on` drives the I/O loop; the I/O that gets spawned by other
    // threads gets polled on this "main" thread.
    let (tx, rx) = futures::channel::oneshot::channel();
    let driver = std::thread::Builder::new()
        .name("Tokio I/O driver".to_string())
        .spawn(move || {
            runtime.block_on(async move {
                rx.await.unwrap();
            });
        })?;

    let ret = with_runtime(move |rt| {
        let _guard = rt.enter();
        let r = f();
        tx.send(()).unwrap();
        r
    });
    driver.join().unwrap();

    let _ = RUNTIME.with(|rt| rt.borrow_mut().take());

    Ok(ret)
}

#[cfg(test)]
mod tests {}
